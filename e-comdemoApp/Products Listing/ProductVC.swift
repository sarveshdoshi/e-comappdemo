//
//  ProductVC.swift
//  e-comdemoApp
//
//  Created by Sarvesh Doshi on 09/08/20.
//  Copyright © 2020 Sarvesh Doshi. All rights reserved.
//

import UIKit

class ProductVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
   
    @CodableUserDefaults(key: "productsListData")
       var productsListData: ProductListing?
   
    var id = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
           initalSetup()
        }
        func initalSetup(){
            tableView.delegate = self
            tableView.dataSource = self
            tableView.tableFooterView = UIView()

        }
}


extension ProductVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsListData?.categories[id].products.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ProductCell
        cell.titleLabel.text = productsListData?.categories[id].products[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "ProductDetails", bundle: .main).instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        vc.categorieId = id
        vc.productId = indexPath.row
        navigationController?.pushViewController(vc, animated: true)
    }
}
