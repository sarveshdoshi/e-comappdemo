//
//  CategoriesVC.swift
//  e-comdemoApp
//
//  Created by Sarvesh Doshi on 09/08/20.
//  Copyright © 2020 Sarvesh Doshi. All rights reserved.
//

import UIKit

class CategoriesVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
   
    @CodableUserDefaults(key: "productsListData")
    var productsListData: ProductListing?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
    }
    func initalSetup(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()

    }
}


extension CategoriesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsListData?.categories.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CategoriesCell
        cell.titleLabel.text = productsListData?.categories[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Products", bundle: .main).instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
        vc.id = indexPath.row
        navigationController?.pushViewController(vc, animated: true)
    }
}
