//
//  ProductDetailsVC.swift
//  e-comdemoApp
//
//  Created by Sarvesh Doshi on 10/08/20.
//  Copyright © 2020 Sarvesh Doshi. All rights reserved.
//

import UIKit

class ProductDetailsVC: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pickColourTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    @CodableUserDefaults(key: "productsListData")
    var productsListData: ProductListing?
    var categorieId = Int()
    var productId = Int()
    var colourPicker = UIPickerView()
    var sortedList: [Variant] = []
    var ColourList: [Variant] = []
    //    var SizeArr = NSMutableArray()
    var SizeArr: [Variant] = []
    var RankArr: [RankingProduct] = []
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setDelegateAndSource()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpScreenData()
        sortedList = productsListData!.categories[categorieId].products[productId].variants
        sortedList.sort(by: { $0.color.compare($1.color) == .orderedAscending})
        ColourList = sortedList.unique{$0.color}
        ColourList.insert(Variant(id: 0, color: "", size: 0, price: 0), at: 0)
        tableView.tableFooterView = UIView()
    }
    func setDelegateAndSource(){
        pickColourTextField.delegate = self
        pickColourTextField.inputView = colourPicker
        colourPicker.delegate = self
        colourPicker.dataSource = self
    }
    
    func setUpScreenData(){
        titleLabel.text = productsListData!.categories[categorieId].products[productId].name
        tableView.isHidden = true
    }
    
}

extension ProductDetailsVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case pickColourTextField:
            print("Tapped")
        default:
            break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
}

extension ProductDetailsVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return ColourList.count
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return ColourList[row].color
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        getDataAsPerColour(color: ColourList[row].color)
    }
    
    func getDataAsPerColour(color: String){
        SizeArr.removeAll()
        for i in 0..<sortedList.count {
            if color == sortedList[i].color{
                self.SizeArr.append(Variant(id: sortedList[i].id,
                                            color: sortedList[i].color,
                                            size: sortedList[i].size,
                                            price: sortedList[i].price))
            }
        }
        print("SizeArray : - " , SizeArr)
        for j in 0..<SizeArr.count {
            //  getRankingDetails(id: 2)
            getRankingDetails(id: SizeArr[j].id)
        }
        tableView.isHidden = false
        tableView.reloadData()
    }
    
    func getRankingDetails(id: Int){
        RankArr.removeAll()
        for i in 0..<(productsListData?.rankings.count)! {
            for j in 0..<(productsListData?.rankings[i].products.count)!{
                if id == productsListData?.rankings[i].products[j].id {
                    self.RankArr.append(RankingProduct(id: productsListData?.rankings[i].products[j].id ?? 0,
                                                       viewCount: productsListData?.rankings[i].products[j].viewCount,
                                                       orderCount: productsListData?.rankings[i].products[j].orderCount,
                                                       shares: productsListData?.rankings[i].products[j].shares))
                }
            }
        }
        print("RankArray:- ", RankArr)
    }
}

extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        return arrayOrdered
    }
}

extension ProductDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SizeArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ProductDetailsCell
        if SizeArr[indexPath.row].size == nil{
            cell.sizeLabel.text = "Size - N/A"
        }else {
            cell.sizeLabel.text = "Size - " + String(describing: SizeArr[indexPath.row].size!)
            
        }
        cell.priceLabel.text = "Price - " + String(describing: SizeArr[indexPath.row].price)
        
        if RankArr.count == SizeArr.count {
            if RankArr[indexPath.row].viewCount == nil {
                cell.viewCountLabel.text = "0"
            }else {
                cell.viewCountLabel.text = String(describing: RankArr[indexPath.row].viewCount!)
            }
            if RankArr[indexPath.row].orderCount == nil{
                cell.orderCountLabel.text = "0"
            }else {
                cell.orderCountLabel.text = String(describing: RankArr[indexPath.row].orderCount!)
            }
            if RankArr[indexPath.row].shares == nil{
                cell.shareCountLabel.text = "0"
            }else {
                cell.shareCountLabel.text = String(describing: RankArr[indexPath.row].shares!)
            }
        }else {
            cell.viewCountLabel.text = "0"
            cell.shareCountLabel.text = "0"
            cell.orderCountLabel.text = "0"
        }
        return cell
    }
}
