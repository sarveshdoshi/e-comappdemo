//
//  ProductDetailsCell.swift
//  e-comdemoApp
//
//  Created by Sarvesh Doshi on 11/08/20.
//  Copyright © 2020 Sarvesh Doshi. All rights reserved.
//

import UIKit

class ProductDetailsCell: UITableViewCell {
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var viewCountLabel: UILabel!
    @IBOutlet weak var orderCountLabel: UILabel!
    @IBOutlet weak var shareCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
