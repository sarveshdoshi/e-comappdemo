//
//  CodableUserDefaults.swift
//  e-comdemoApp
//
//  Created by Sarvesh Doshi on 09/08/20.
//  Copyright © 2020 Sarvesh Doshi. All rights reserved.
//

import Foundation

@propertyWrapper
public struct CodableUserDefaults<Value: Codable> {

    public struct Key:
        ExpressibleByStringLiteral,
        ExpressibleByStringInterpolation
    {

        public let rawValue: String

        public init(stringLiteral value: String) {
            self.rawValue = value
        }

    }

    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dataDecodingStrategy = .base64
        decoder.dateDecodingStrategy = .iso8601
        decoder.nonConformingFloatDecodingStrategy = .convertFromString(
            positiveInfinity: "Infinity",
            negativeInfinity: "-Infinity",
            nan: "NaN"
        )
        return decoder
    } ()

    private let encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dataEncodingStrategy = .base64
        encoder.dateEncodingStrategy = .iso8601
        encoder.nonConformingFloatEncodingStrategy = .convertToString(
            positiveInfinity: "Infinity",
            negativeInfinity: "-Infinity",
            nan: "NaN"
        )
        return encoder
    } ()

    private let key: Key
    private let valueFor: (String) -> Any?
    private let setValue: (Value, String) -> Void
    private let removeValue: () -> Void

    public init(wrappedValue: Value?, key: Key) {
        self.init(key: key)
        self.wrappedValue = wrappedValue
    }

    public init(key: Key) {
        self.key = key

        let userDefaults = UserDefaults.standard

        if Value.self is UserDefaultsRepresentable.Type {
            valueFor = userDefaults.value(forKey:)
            setValue = userDefaults.setValue(_:forKey:)
        } else {
            valueFor = { [weak decoder, weak userDefaults] key in
                userDefaults?.data(forKey: key).flatMap {
                    try? decoder?.decode(Value?.self, from: $0)
                }
            }
            setValue = { [weak encoder, weak userDefaults] value, key in
                userDefaults?.set(try? encoder?.encode(value), forKey: key)
            }
        }
        removeValue = { [weak userDefaults] in
            userDefaults?.removeObject(forKey: key.rawValue)
        }
    }

    public var wrappedValue: Value? {
        get { valueFor(key.rawValue) as? Value }
        set {
            switch newValue {
                case .some(let value): setValue(value, key.rawValue)
                case .none: removeValue()
            }
        }
    }

}

fileprivate protocol    UserDefaultsRepresentable {}

extension   Int:        UserDefaultsRepresentable {}

extension   Double:     UserDefaultsRepresentable {}

extension   Float:      UserDefaultsRepresentable {}

extension   Bool:       UserDefaultsRepresentable {}

extension   String:     UserDefaultsRepresentable {}

extension   Data:       UserDefaultsRepresentable {}

extension   Array:      UserDefaultsRepresentable
    where   Element:    UserDefaultsRepresentable {}

extension   Dictionary: UserDefaultsRepresentable
    where   Key ==      String,
            Value:      UserDefaultsRepresentable {}

