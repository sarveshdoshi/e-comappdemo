//
//  DashBoardVC.swift
//  e-comdemoApp
//
//  Created by Sarvesh Doshi on 08/08/20.
//  Copyright © 2020 Sarvesh Doshi. All rights reserved.
//

import UIKit

class DashBoardVC: UIViewController {

    @CodableUserDefaults(key: "productsListData")
    var productsListData: ProductListing?

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchProdcut()
    }
    
    @IBAction func CategoriesButtonTapped(_ sender: UIButton) {
        // open the categories Listing the button Tap
        let vc = UIStoryboard(name: "Categories", bundle: .main).instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
        navigationController?.pushViewController(vc, animated: true)
    }

    func fetchProdcut() {
        URLSession.shared.dataTask(with: URL(string: "https://stark-spire-93433.herokuapp.com/json")!) { (data, response, err) in
            guard let data = data else { return }
            do{
                print("Success Here")
                let apiData = try? JSONDecoder().decode(ProductListing.self, from: data)
                self.productsListData = apiData
            }
        }.resume()
    }
    
}
