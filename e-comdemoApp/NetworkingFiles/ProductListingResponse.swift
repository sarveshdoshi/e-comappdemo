//
//  ProductListingResponse.swift
//  e-comdemoApp
//
//  Created by Sarvesh Doshi on 08/08/20.
//  Copyright © 2020 Sarvesh Doshi. All rights reserved.
//

import Foundation


// MARK: - ProductListing
struct ProductListing: Codable {
    var categories: [Category]
    var rankings: [Ranking]
}

// MARK: - Category
struct Category: Codable {
    var id: Int
    var name: String
    var products: [CategoryProduct]
    var childCategories: [Int]

    enum CodingKeys: String, CodingKey {
        case id, name, products
        case childCategories = "child_categories"
    }
}

// MARK: - CategoryProduct
struct CategoryProduct: Codable {
    var id: Int
    var name, dateAdded: String
    var variants: [Variant]
    var tax: Tax

    enum CodingKeys: String, CodingKey {
        case id, name
        case dateAdded = "date_added"
        case variants, tax
    }
}

// MARK: - Tax
struct Tax: Codable {
    var name: String
    var value: Double
}

//enum Name: String, Codable {
//    case vat = "VAT"
//    case vat4 = "VAT4"
//}

// MARK: - Variant
struct Variant: Codable {
    var id: Int
    var color: String
    var size: Int?
    var price: Int
}

// MARK: - Ranking
struct Ranking: Codable {
    var ranking: String
    var products: [RankingProduct]
}

// MARK: - RankingProduct
struct RankingProduct: Codable {
    var id: Int
    var viewCount, orderCount, shares: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case viewCount = "view_count"
        case orderCount = "order_count"
        case shares
    }
}
